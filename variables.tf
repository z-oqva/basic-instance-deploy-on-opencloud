# Ваш Access Key
variable "accesskey" {
  default = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}
# Ваш Secret
variable "secretkey" {
  default = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}
variable "zcloud_ip" {
  default = "console.opencloud.uz"
}
# Необходимое количество серверов
variable "quantity-server" {
  default = 1
}
# Hostname инстанса
variable "servername" {
  default = "servertest-"
}
# Название ключа - https://console.opencloud.uz/compute/key-pairs
variable "keyname" {
  default = "test-opencloud"
}
# Тип инстанса, варианты - https://console.opencloud.uz/compute/instance-types
variable "instance" {
  default = "z2.medium"
}
# ID образа, ниже пример образа Ubuntu 22.04 LTS - Jammy Jellyfish
variable "ami" {
    default = "ami-63476ce1b8b84da29104e3bfc0d4a32f"
}
# ID вашего VPC
variable "vpc" {
  default = "vpc-4xxxxxxxxxxxxxxxxxxxxxxxxxx"
}
# ID вашего сабнета
variable "subnet" {
  default = "subnet-xxxxxxxxxxxxxxxxxxxxxxxxxx"
}