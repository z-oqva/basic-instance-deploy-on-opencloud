# Пример установки тестового инстанса на OpenCloud UZ
Пример сценария для развертывания Ubuntu Linux на OpenCloud UZ (количество можно поменять в variables.tf)


## Подготовка
Чтобы подготовить скрипт к работе с вашими учетными данными и переменными, обновите файл variables.tf
Необходимо создать новый Access Key здесь - https://console.opencloud.uz/my-profile-and-security/access-keys

## Запуск
В этом примере использовался официальный провайдер Hashicorp AWS  https://registry.terraform.io/providers/hashicorp/aws/latest/docs

После настройки файла variables.tf вы должны выполнить следующие команды
- terraform init
- terraform plan
- terraform apply


## Details
- connect.tf - общие параметры подключения к OpenCloud UZ
- network.tf Создает новый security group (если необходимо)
- server.tf - деплой инстанса